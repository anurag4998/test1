﻿using gateway.api.Logger;
using gateway.api.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Micro.Common.Models;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;

namespace gateway.api.Services
{
    public class Rule1Service:Controller, IRule1Service
    {
        private readonly IHttpClientFactory _httpClientFactory;
     
        public Rule1Service(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Datalogger> VerifyRule1(int jobId, int ContractId, int ruleId)
        {
            var client = _httpClientFactory.CreateClient("g");
            var request_rule_1 = new HttpRequestMessage(HttpMethod.Get, "http://localhost:5050");
            var response =  await client.SendAsync(request_rule_1);
        
            var result = JsonConvert.DeserializeObject<Result>(response.Content.ReadAsStringAsync().Result);

            Datalogger _datalogger = new Datalogger(jobId, ContractId, ruleId, result);
           

           
            return _datalogger;
        }
       

    }
}   
