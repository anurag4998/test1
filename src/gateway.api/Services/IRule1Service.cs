﻿using Micro.Common.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gateway.api.Services
{
    interface IRule1Service
    {
        public Task<Datalogger> VerifyRule1(int jobId, int ContractId, int ruleId);
    }
}
