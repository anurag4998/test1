﻿using gateway.api.Logger;
using Micro.Common.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace gateway.api.Services
{
    public class Rule2Service: Controller, IRule2Service
    {
        private readonly IHttpClientFactory _httpClientFactory;

        public Rule2Service(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Datalogger> VerifyRule2(int jobId, int ContractId, int ruleId)
        {
            var client = _httpClientFactory.CreateClient();
            var request_rule_2 = new HttpRequestMessage(HttpMethod.Get, "http://localhost:5051");

            var response = await client.SendAsync(request_rule_2);

            var result = JsonConvert.DeserializeObject<Result>(response.Content.ReadAsStringAsync().Result);

            Datalogger _datalogger = new Datalogger(jobId, ContractId, ruleId, result);

            return _datalogger;
        }
    }
}
