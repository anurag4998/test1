﻿using Micro.Common.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gateway.api.Services
{
    interface IRule2Service
    {
        public Task<Datalogger> VerifyRule2(int jobId,int ContractId, int ruleId);
    }
}
