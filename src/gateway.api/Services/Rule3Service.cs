﻿using gateway.api.Logger;
using Micro.Common.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace gateway.api.Services
{
    public class Rule3Service: Controller, IRule3Service
    {
        
        private readonly IHttpClientFactory _httpClientFactory;

        public Rule3Service(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<Datalogger> VerifyRule3(int jobId, int ContractId, int ruleId)
        {
            var client = _httpClientFactory.CreateClient();
            var request_rule_3 = new HttpRequestMessage(HttpMethod.Get, "http://localhost:5052");

            var response = await client.SendAsync(request_rule_3);
            var result = JsonConvert.DeserializeObject<Result>(response.Content.ReadAsStringAsync().Result);

            Datalogger _datalogger = new Datalogger(jobId, ContractId, ruleId, result);
            return _datalogger;
        }
    }
}

