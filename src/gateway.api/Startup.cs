using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using gateway.api.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.EntityFrameworkCore;
using gateway.api.Models;
using gateway.api.Logger;
using Micro.Common.Models;
using ClosedXML.Excel;

namespace gateway.api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        private readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(Datalogger));

        public void ConfigureServices(IServiceCollection services)
        {

           
           
            services.AddDbContext<Microcontext>(options =>
               options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));


            services.AddControllers();
            services.AddHttpClient();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "gateway.api", Version = "v1" });
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "gateway.api v1"));
            }

           

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            loggerFactory.AddLog4Net();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            IHttpClientFactory _clientFactory = app.ApplicationServices.GetService(typeof(IHttpClientFactory)) as IHttpClientFactory;


            lifetime.ApplicationStarted.Register(OnApplicationStartedAsync(_clientFactory).Wait);
        }

        private async Task<Action> OnApplicationStartedAsync(IHttpClientFactory httpClientFactory)
        {
            Rule1Service rule1Service = new Rule1Service(httpClientFactory);
            Rule2Service rule2Service = new Rule2Service(httpClientFactory);
            Rule3Service rule3Service = new Rule3Service(httpClientFactory);
            
           try
            {
               
                var optionsBuilder = new DbContextOptionsBuilder<Microcontext>();
                optionsBuilder.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"));
                List<Task<Datalogger>> tasks = new List<Task<Datalogger>>();
                
                log4net.Config.XmlConfigurator.Configure(); //configure
                Console.WriteLine("The application started at {0:HH:mm:ss.fff}", DateTime.Now);

                using (var context = new Microcontext(optionsBuilder.Options))
                {

                    var jobs = context.Job.ToList();
                    foreach (var job in jobs)
                    {
                        var rules = context.Rule.Where(x => x.JobId == job.JobId).ToList();
                                           
                        foreach (var rule in rules)
                        {
                            switch (rule.RuleId)
                            {
                                case 1:

                                    tasks.Add(rule1Service.VerifyRule1(job.JobId,Convert.ToInt32(job.ContractId),rule.RuleId));
                                    break;

                                case 2:
                                    tasks.Add(rule2Service.VerifyRule2(job.JobId, Convert.ToInt32(job.ContractId), rule.RuleId));
                                    break;

                                case 3:
                                    tasks.Add(rule3Service.VerifyRule3(job.JobId, Convert.ToInt32(job.ContractId), rule.RuleId));
                                    break;

                                default:
                                    break;
                            }

                        }
                    }
                }

                var results = await Task.WhenAll(tasks);
                Console.WriteLine("The application ended at {0:HH:mm:ss.fff}", DateTime.Now);

                Console.WriteLine("Job Id" + "\t" + "Contract Id" + "\t" + "Rule Id" + "\t" + "Steps");
                using (var workbook = new XLWorkbook())
                {
                    var worksheet = workbook.Worksheets.Add("Users");
                    var currentRow = 1;

                    worksheet.Cell(currentRow, 1).Value = "Job Id";
                    worksheet.Cell(currentRow, 2).Value = "Contract Id";
                    worksheet.Cell(currentRow, 3).Value = "Rule Id";
                    worksheet.Cell(currentRow, 4).Value = "Step Description";
                    worksheet.Cell(currentRow, 5).Value = "Status";

                    foreach (var result in results)
                    {
                        foreach (var step in result.Results.StepDescription)
                        {
                            log.Info(step.Status);
                            Console.Write(result.JobId + "\t");
                            Console.Write(result.ContractId + "\t");
                            Console.Write(result.RuleId + "\t");
                            Console.Write(step.Description + "\t");
                            Console.WriteLine(step.Status + "\t");

                            currentRow++;
                            worksheet.Cell(currentRow, 1).Value = result.JobId;
                            worksheet.Cell(currentRow, 2).Value = result.ContractId;
                            worksheet.Cell(currentRow, 3).Value = result.RuleId;
                            worksheet.Cell(currentRow, 4).Value = step.Description;
                            worksheet.Cell(currentRow, 5).Value = step.Status;

                            //logger.Info("Starting the application...");


                        }
                        currentRow++;
                        worksheet.Row(currentRow).Style.Fill.SetBackgroundColor(XLColor.Yellow);
                        worksheet.Cell(currentRow, 1).Value = result.JobId;
                        worksheet.Cell(currentRow, 2).Value = result.ContractId;
                        worksheet.Cell(currentRow, 3).Value = result.RuleId;
                        worksheet.Cell(currentRow, 5).Value = result.Status;

                        workbook.SaveAs("Datalog.xlsx");

                        Console.Write(result.Results.Status);
                        Console.WriteLine();

                    }
                }

                Console.WriteLine("try");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.ToString());
            }

                      
            return null;
        }
      
    }
}

